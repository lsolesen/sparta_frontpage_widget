<?php
/**
 * @file
 * sparta_frontpage_widget.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function sparta_frontpage_widget_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'front_widgets';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Front Widgets';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Front Widgets';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '3';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'grid';
  $handler->display->display_options['style_options']['columns'] = '3';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Content: Target URL */
  $handler->display->display_options['fields']['field_target_url']['id'] = 'field_target_url';
  $handler->display->display_options['fields']['field_target_url']['table'] = 'field_data_field_target_url';
  $handler->display->display_options['fields']['field_target_url']['field'] = 'field_target_url';
  $handler->display->display_options['fields']['field_target_url']['label'] = '';
  $handler->display->display_options['fields']['field_target_url']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_target_url']['element_label_colon'] = FALSE;
  /* Field: Content: Image */
  $handler->display->display_options['fields']['field_widget_image']['id'] = 'field_widget_image';
  $handler->display->display_options['fields']['field_widget_image']['table'] = 'field_data_field_widget_image';
  $handler->display->display_options['fields']['field_widget_image']['field'] = 'field_widget_image';
  $handler->display->display_options['fields']['field_widget_image']['label'] = '';
  $handler->display->display_options['fields']['field_widget_image']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_widget_image']['alter']['text'] = '<a href="[field_target_url]">[field_widget_image]</a>';
  $handler->display->display_options['fields']['field_widget_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_widget_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_widget_image']['settings'] = array(
    'image_style' => '3-4-col-widget',
    'image_link' => '',
  );
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['text'] = '<h4 class="field-content"><a href="[field_target_url]">[title]</a></h4>';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_type'] = 'h4';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'front_widget' => 'front_widget',
  );

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $export['front_widgets'] = $view;

  return $export;
}
