<?php
/**
 * @file
 * sparta_frontpage_widget.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function sparta_frontpage_widget_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function sparta_frontpage_widget_views_api() {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function sparta_frontpage_widget_image_default_styles() {
  $styles = array();

  // Exported image style: 3-4-col-widget.
  $styles['3-4-col-widget'] = array(
    'name' => '3-4-col-widget',
    'effects' => array(
      5 => array(
        'label' => 'Scale and crop',
        'help' => 'Scale and crop will maintain the aspect-ratio of the original image, then crop the larger dimension. This is most useful for creating perfectly square thumbnails without stretching the image.',
        'effect callback' => 'image_scale_and_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_resize_form',
        'summary theme' => 'image_resize_summary',
        'module' => 'image',
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => '380',
          'height' => '200',
        ),
        'weight' => '1',
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function sparta_frontpage_widget_node_info() {
  $items = array(
    'front_widget' => array(
      'name' => t('Front Widget'),
      'base' => 'node_content',
      'description' => t('These are the four sections immediately below the image slider on the home page.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
